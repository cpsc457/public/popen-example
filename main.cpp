#include <cstdio>
#include <cstdlib>
#include <string>

void usage( const std::string & pname, int exit_code)
{
  printf( "Usage: %s file1 [file2 [file3 ...]]\n", pname.c_str());
  exit( exit_code);
}

int main( int argc, char ** argv)
{
  if( argc < 2) usage( argv[0], -1);

  for( int i = 1 ; i < argc ; i ++) {
    // I like C++ strings better than C strings, they waste more memory
    std::string fname = argv[i];
    // prepare the command to execute in popen()
    std::string cmd = "file -b " + fname;
    // run the command in popen()
    FILE * fp = popen( cmd.c_str(), "r");
    if( fp == nullptr) {
      printf("popen() failed, quitting\n");
      exit(-1);
    }
    // get the output of popen() using fgets(3) into raw C-style char buffer
    std::string ftype;
    char buff[4096];
    char * res = fgets(buff, sizeof(buff), fp);
    pclose(fp);
    // try to parse the buffer
    if( res != nullptr) {
      // find the end of the first field ('\0', or '\n' or ',')
      int eol = 0;
      while(buff[eol] != ',' && buff[eol] != '\n' && buff[eol] != 0) eol ++;
      // terminate the string
      buff[eol] = 0;
      // remember the type
      ftype = buff;
    } else {
      // file(1) did not produce any output... no idea why, so let's
      // just skip the file, but make the file type something bizare
      ftype = "file(1) failed, not sure why";
    }
    printf("%20s: %s\n", fname.c_str(), ftype.c_str());
  }

  return 0;
}
